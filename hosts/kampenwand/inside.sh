#!/usr/bin/env bash

set -ex

source "base/users.sh"

apt-get install -y --no-install-recommends haproxy 

source "base/secrets.sh"
secret_get "HAPROXY_BASICAUTH_MONITOR"
export HAPROXY_BASICAUTH_MONITOR
secret_get "HAPROXY_BASICAUTH_MIGRATION"
export HAPROXY_BASICAUTH_MIGRATION
secret_get "HAPROXY_BASICAUTH_ADMIN"
export HAPROXY_BASICAUTH_ADMIN
install_template "kampenwand" "/etc/haproxy/haproxy.cfg" || haproxy -c -f /etc/haproxy/haproxy.cfg && service haproxy reload

