#!/usr/bin/env bash

set -ex

source "base/users.sh"
source "base/base.sh"
setup_sshd

user_setup "dachary" "sudo"
user_setup "gapodo" "sudo"
# inside configuration will be done in Ansible.
