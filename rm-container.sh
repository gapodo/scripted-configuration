#!/usr/bin/env bash

source "base/base.sh"

if ! confirm "Do you really want to remove the container?"; then exit 1; fi
if ! confirm "Are you aware that the root fs will be lost, too? Remember to save your WIP Git config :D"; then exit 1; fi

CONTAINER="${1}"
lxc-stop "${CONTAINER}"
lxc-destroy "${CONTAINER}" && git remote rm "${CONTAINER}" 
